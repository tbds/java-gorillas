/**
 * Default main class/entry point for the game
 */
public class gorilla
{
    /**
     * Main class
     */
    public static void main(String[] args)
    {
        Controller controller = new Controller();
        controller.start();
    }
}
