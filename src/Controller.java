import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextCharacter;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.InputProvider;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;
import java.util.Random;

/**
 * Class for controlling the game
 */
class Controller
{
    /**
     * The number of columsn on the screen
     */
    protected int columns;
    /**
     * The players that are competing in the game
     */
    protected Player[] players;
    /**
     * The number of rows on the screen
     */
    protected int rows;
    /**
     * The screen object itself
     */
    protected TerminalScreen screen;
    /**
     * The text graphics
     */
    protected TextGraphics textGraphics;

    /**
     * The speed the wind is travelling at
     */
    protected double windSpeed;

    /**
     * Function that initiates the game
     */
    public void start()
    {
		DefaultTerminalFactory defaultTerminalFactory = new DefaultTerminalFactory();
        this.players = new Player[2];
        this.players[0] = new Player();
        this.players[1] = new Player();

		try {
			Terminal terminal = defaultTerminalFactory.createTerminal();
			this.screen = new TerminalScreen(terminal);
			this.screen.startScreen();
			this.screen.setCursorPosition(null);
			this.textGraphics = this.screen.newTextGraphics();
			this.columns = this.screen.getTerminalSize().getColumns();
			this.rows = this.screen.getTerminalSize().getRows();

            this.setUpGame();

			drawWorld();
			startGame();
		} catch (Exception e) {
            /**
             * @TODO
             * Probably want debug parameters to determine whether this gets
             * printed out or not
             */
            System.out.println(e.getMessage());
		}
    }

	protected void drawWorld() throws Exception {
		drawSky();
		drawNames();
		drawBuildings();
		drawGorillas();
	}

	protected void drawNames() throws Exception {
        String player1Name = this.players[0].getName();
        String player2Name = this.players[1].getName();

		textGraphics.setForegroundColor(new TextColor.RGB(255, 255, 255));
		textGraphics.setBackgroundColor(TextColor.ANSI.BLUE);
		textGraphics.putString(0, 0, player1Name);
		textGraphics.putString(
             columns - 1 - player2Name.length(), 0, player2Name
        );
	}

	protected void drawSky() throws Exception {
		textGraphics.setForegroundColor(new TextColor.RGB(255, 255, 0));
		textGraphics.setBackgroundColor(TextColor.ANSI.BLUE);
		textGraphics.fill(' ');
		textGraphics.setCharacter((int) Math.round(columns / 2), 0, '\u263C');
		this.screen.refresh();
	}

	protected void drawBuildings() throws Exception {
		TerminalSize buildingSize;
		int xStdDev = 2;
		int yStdDev = 3;
		int yMed = rows / 2 - 4;
		int yMin = 2;
		int yMax = rows - 2;
		int xPosition = 0, yPosition;
		int xSize, ySize;
		int randInt;
		char buildingWindow = '\u220E';
		Random random = new Random();
		double randDouble;

		do {
			do {
				randDouble = random.nextGaussian();
				xSize = (int) Math.round(Math.random() * xStdDev + 4);
				randDouble = random.nextGaussian();
				ySize = (int) Math.round(randDouble * yStdDev + yMed);
			} while (ySize < yMin || ySize > yMax);

			yPosition = rows - ySize;
			buildingSize = new TerminalSize((int) xSize, (int) ySize);
			textGraphics.setForegroundColor(new TextColor.RGB(255, 255, 82));
			randInt = random.nextInt(3);
			textGraphics.setBackgroundColor((randInt == 0) ? TextColor.ANSI.RED : (randInt == 1) ? TextColor.ANSI.CYAN : new TextColor.RGB(173, 170, 173));
			textGraphics.fillRectangle(new TerminalPosition(xPosition, yPosition), buildingSize, buildingWindow);
			xPosition += xSize;
		} while (xPosition < columns);

		this.screen.refresh();
	}

	protected void drawGorillas() throws Exception {
		int xMed = Math.round(columns / 6);
		int xStdDev = Math.round(columns / 12);
		int xPosition;
		char buildingWindow = '\u220E';
		TerminalSize gorillaSize = new TerminalSize(2, 2);
		textGraphics.setForegroundColor(TextColor.ANSI.BLUE);
		textGraphics.setBackgroundColor(new TextColor.RGB(255, 170, 82));

		for (int player = 0; player < 2; player++) {
			xPosition = (player == 0) ? (int) Math.round((Math.random() - 0.5) * xStdDev + xMed) : (int) Math.round((Math.random() - 0.5) * xStdDev + columns - xMed);

			for (int yPosition = 0; yPosition < rows; yPosition++) {
				if (this.screen.getBackCharacter(xPosition, yPosition + 2).getCharacter() == buildingWindow || this.screen.getBackCharacter(xPosition + 1, yPosition + 2).getCharacter() == buildingWindow) {
					if (player == 0 && this.screen.getBackCharacter(xPosition - 1, yPosition).getCharacter() == buildingWindow) {
						xPosition++;
						yPosition = 0;
						continue;
					}

					if (player == 1 && this.screen.getBackCharacter(xPosition + 2, yPosition).getCharacter() == buildingWindow) {
						xPosition--;
						yPosition = 0;
						continue;
					}

					if (this.screen.getBackCharacter(xPosition, yPosition + 2).getCharacter() == buildingWindow ^ this.screen.getBackCharacter(xPosition + 1, yPosition + 2).getCharacter() == buildingWindow) {
						xPosition = (player == 0) ? (int) Math.round((Math.random() - 0.5) * xStdDev + xMed) : (int) Math.round((Math.random() - 0.5) * xStdDev + columns - xMed);
						yPosition = 0;
						continue;
					} else {
						textGraphics.fillRectangle(new TerminalPosition(xPosition, yPosition), gorillaSize, 'G');
						if (player == 0) {
                            this.players[player].setOrigin(xPosition - 1, yPosition);
						} else {
                            this.players[player].setOrigin(xPosition + 2, yPosition);
						}
					}
					break;
				}
			}
		}

		this.screen.refresh();
	}

	protected void startGame() throws Exception {
		int activePlayer = 0;
		setWind();

		while (this.players[0].isAlive() && this.players[1].isAlive()) {
			takeTurn(activePlayer);
			activePlayer = (activePlayer + 1) % 2;
		}

        this.displayVictoryScreen();

        Thread.sleep(2000);

        this.displayContinueScreen();
        if(this.getContinueOption()) {
            this.start();
        }


		this.screen.close();
	}

	protected void setWind() throws Exception {
		int windSpeedStdDev = 5;
		int xMed = Math.round(columns / 2);
		double windSpeed = new Random().nextGaussian() * windSpeedStdDev;
		int lineLength = (int) Math.round(windSpeed + xMed);

		textGraphics.setForegroundColor(TextColor.ANSI.RED);
		textGraphics.setBackgroundColor(TextColor.ANSI.BLUE);
		textGraphics.fillRectangle(new TerminalPosition(0, rows - 1), new TerminalSize(columns, 1), ' ');
		textGraphics.drawLine(Math.round(columns / 2), rows - 1, lineLength, rows - 1, '\u2500');

		if (windSpeed > 0) {
			textGraphics.setCharacter(lineLength + 1, rows - 1, '\u25B6');
		} else {
			textGraphics.setCharacter(lineLength - 1, rows - 1, '\u25C0');
		}

		this.screen.refresh();
        this.windSpeed = windSpeed;
	}

	protected void takeTurn(int activePlayer) throws Exception {
		int angle, velocity;
		TerminalPosition cursorPos;

		textGraphics.setForegroundColor(new TextColor.RGB(255, 255, 255));
		textGraphics.setBackgroundColor(TextColor.ANSI.BLUE);

		cursorPos = new TerminalPosition((activePlayer == 0) ? 0 : columns - 14, 1);
		textGraphics.putString(cursorPos, "Angle: ");
		cursorPos = cursorPos.withRelativeColumn(7);
		this.screen.setCursorPosition(cursorPos);
		this.screen.refresh();

		angle = getInput(cursorPos);

		cursorPos = new TerminalPosition((activePlayer == 0) ? 0 : columns - 14, 2);
		textGraphics.putString(cursorPos, "Velocity: ");
		cursorPos = cursorPos.withRelativeColumn(10);
		this.screen.setCursorPosition(cursorPos);
		this.screen.refresh();

		velocity = getInput(cursorPos);

		throwBanana(activePlayer, angle, velocity);
		drawNames();

		textGraphics.fillRectangle(new TerminalPosition(0, 1), new TerminalSize(columns, 2), ' ');
	}

    /**
     * Get input from the user, this forces it to be an integer
     */
    protected int getInput(TerminalPosition cursorPos) throws Exception {
        return Integer.parseInt(this.getInput(cursorPos, 3, false));
    }

	protected String getInput(
        TerminalPosition cursorPos,
        int maxLength,
        boolean allowCharacters
    ) throws Exception {
		KeyStroke key;
        String input = "";

		while (true) {
			key = this.screen.readInput();
			if (
                key.getKeyType() == KeyType.Character &&
                input.length() < maxLength &&
                (allowCharacters || Character.isDigit(key.getCharacter()))
            ) {
                input += key.getCharacter();
				textGraphics.setCharacter(cursorPos, key.getCharacter());
				cursorPos = cursorPos.withRelativeColumn(1);
				this.screen.setCursorPosition(cursorPos);
			} else if (key.getKeyType() == KeyType.Backspace && input.length() > 0) {
                input = input.substring(0, input.length()-1);
				cursorPos = cursorPos.withRelativeColumn(-1);
				textGraphics.setCharacter(cursorPos, ' ');
				this.screen.setCursorPosition(cursorPos);
			} else if (key.getKeyType() == KeyType.Enter && input.length() > 0) {
				cursorPos = cursorPos.withRelativeColumn(-1);
				break;
			} else if (key.getKeyType() == KeyType.Escape || key.getCharacter() == 'q') {
				this.screen.close();
				System.exit(0);
			}

			this.screen.refresh();
		}

		return input;
	}

	protected void throwBanana(int activePlayer, int angle, int velocity) throws Exception {
		char buildingWindow = '\u220E';
		TerminalPosition position;
		double xPos = this.players[activePlayer].getX();
		double yPos = this.players[activePlayer].getY();
		double xVeloc = (activePlayer == 0) ? Math.cos(Math.toRadians(angle)) * velocity : Math.cos(Math.toRadians(angle)) * velocity * -1.0;
		double yVeloc = Math.sin(Math.toRadians(angle)) * velocity * -1.0;
		double gravity = 9.80665;

		int fps = 60;
		double frameDuration = 1.0 / (double) fps;
		double frameDurationInMillis = 1000.0 / fps;
		long frameMillis = (long) frameDurationInMillis;
		int frameNanos = (int) (1000000.0 * (frameDurationInMillis - frameMillis));

		textGraphics.setForegroundColor(TextColor.ANSI.BLUE);
		textGraphics.setBackgroundColor(new TextColor.RGB(255, 170, 82));

		position = new TerminalPosition((int) xPos, (int) yPos);
		textGraphics.setCharacter(position, 'G');

		textGraphics.setForegroundColor(new TextColor.RGB(255, 255, 0));
		textGraphics.setBackgroundColor(TextColor.ANSI.BLUE);

		position = position.withRelativeRow(-1);
		textGraphics.setCharacter(position, 'b');
		this.screen.refresh();

		Thread.sleep(frameMillis, frameNanos);
		position = position.withRelativeRow(1);
		textGraphics.setCharacter(position, ' ');
		position = position.withRelativeRow(-1);
		yPos -= 1;

		while (true) {
			Thread.sleep(frameMillis, frameNanos);
			xPos += xVeloc * frameDuration;
			yPos += yVeloc * frameDuration;
			xVeloc += frameDuration * this.windSpeed;
			yVeloc += frameDuration * gravity;

			textGraphics.setCharacter(position, ' ');
			this.screen.refresh();

			if (xPos <= 0 || xPos > columns - 1|| yPos > rows - 1) {
				break;
			}

			if (yPos <= 0) {
				continue;
			}

			position = new TerminalPosition((int) Math.round(xPos), (int) Math.round(yPos));

			if (this.screen.getBackCharacter(position).getCharacter() == buildingWindow) {
				textGraphics.setCharacter(position, ' ');
				this.screen.refresh();
				break;
			} else if (this.screen.getBackCharacter(position).getCharacter() == 'G') {
				textGraphics.setCharacter(position, 'b');

				if (position.getColumn() > columns / 2) {
					this.players[1].kill();
				} else {
					this.players[0].kill();
				}

				break;
			} else if (this.screen.getBackCharacter(position).getCharacter() == ' ') {
				textGraphics.setCharacter(position, 'b');
			}

			this.screen.refresh();
		}
	}

    /**
     * Prompt the user for necessary elements to start up the game
     */
    protected void setUpGame() throws Exception {
        for(int counter = 0; counter < 2; counter++) {
            this.textGraphics.setBackgroundColor(TextColor.ANSI.BLUE);
            this.textGraphics.setForegroundColor(
                    new TextColor.RGB(255, 255, 0)
            );
            textGraphics.fill(' ');
            String tempName;
            if(counter == 0) {
                tempName = "one";
            } else {
                tempName = "two";
            }
            String prompt = "Enter player "+ tempName +"'s name: ";
            this.textGraphics.putString(0, 0, prompt);

            TerminalPosition cursorPos;
            cursorPos = new TerminalPosition(0, prompt.length());
            this.screen.setCursorPosition(cursorPos);
            this.screen.refresh();

            String name = getInput(cursorPos, 50, true);
            this.players[counter].setName(name);
        }
    }

    /**
     * The game is over, figure out who won and display an appropriate message
     */
    protected void displayVictoryScreen() {
        try {
            this.textGraphics.setBackgroundColor(new TextColor.RGB(255, 255, 0));
            this.textGraphics.setForegroundColor(TextColor.ANSI.BLACK);
            this.textGraphics.fill(' ');

            String message;
            if (this.players[0].isAlive()) {
                message = this.players[0].getName();
            } else {
                message = this.players[1].getName();
            }
            message += " wins!!!";
            this.textGraphics.putString(
                    Math.round(this.columns / 2) - (int)message.length() / 2,
                    Math.round(this.rows / 2),
                    message
            );
            this.screen.setCursorPosition(null);
            this.screen.refresh();
        } catch (Exception e) {
            /**
             * @TODO
             * Probably want debug parameters to determine whether this gets
             * printed out or not. Probably also want to consolidate this
             * to avoid duplicate code
             */
            System.out.println(e.getMessage());
        }
    }

    /**
     * Gives the users the option to continue playing another game or not
     */
    protected void displayContinueScreen() {
        try {
            String message = "Play again? (Y/N)";
            this.textGraphics.putString(
                Math.round(this.columns / 2) - (int)message.length() / 2,
                Math.round(this.rows / 2) + 3,
                message
            );
            this.screen.refresh();
        } catch (Exception e) {
            /**
             * @TODO
             * Probably want debug parameters to determine whether this gets
             * printed out or not. Probably also want to consolidate this
             * to avoid duplicate code
             */
            System.out.println(e.getMessage());
        }
    }

    /**
     * Wait for the user to enter whether they want to continue or not
     */
    protected boolean getContinueOption() throws Exception {
        KeyStroke key;
        char lowerCaseCharacter;

        do {
            key = this.screen.readInput();
            lowerCaseCharacter = Character.toLowerCase(key.getCharacter());
        } while (lowerCaseCharacter != 'y' && lowerCaseCharacter != 'n');
        return lowerCaseCharacter == 'y';
    }
}
