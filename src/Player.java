/**
 * Class for controlling the players
 *
 * @TODO Convert!!!!
 */
class Player {
    protected String name;

	private int xBananaOrigin, yBananaOrigin;
	private boolean alive;
	
	public Player() {
        this.initialSetup(0, 0);
	}
	
	public Player(int xBananaOrigin, int yBananaOrigin) {
        this.initialSetup(xBananaOrigin, yBananaOrigin);
	}
	
	public void setOrigin(int xBananaOrigin, int yBananaOrigin) {
		this.xBananaOrigin = xBananaOrigin;
		this.yBananaOrigin = yBananaOrigin;
	}
	
	public int getX() {
		return xBananaOrigin;
	}
	
	public int getY() {
		return yBananaOrigin;
	}
	
	public void kill() {
		alive = false;
	}
	
	public boolean isAlive() {
		return alive;
	}

    /**
     * Returns the name of this player
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * Sets the name of this player
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Set up the initial parameters including the x & y for the banana
     * origin
     */
    protected void initialSetup(int xBananaOrigin, int yBananaOrigin)
    {
		this.xBananaOrigin = xBananaOrigin;
		this.yBananaOrigin = yBananaOrigin;
		this.alive = true;
        this.name = "A player";
    }
}
