# Java Gorillas!

This is a Java and CLI version of the classic Q-Basic game: Gorillas!

## Requirements
* Java Compiler (JDE)

## Compiling
* Run the compile script: `./compile`

## Running
* `./run`
